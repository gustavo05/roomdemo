package com.example.roomdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.movie_cell_layout.view.*

class MovieAdapter() : RecyclerView.Adapter<MovieAdapter.MovieCellViewHolder>() {

    private var dataSource: List<Any> = listOf()

    override fun getItemCount(): Int = dataSource.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieCellViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_cell_layout, parent, false)
        return MovieCellViewHolder(view)
    }


    override fun onBindViewHolder(holder: MovieCellViewHolder, position: Int) {
        holder.bind()
    }

    fun submitData(list: List<Any>) {
        dataSource = list
        notifyDataSetChanged()
    }


    class MovieCellViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind() {
            with(itemView) {
                movie_textView.text = "Avengers"
                director_textView.text = "Joe Russo"
            }
        }
    }
}